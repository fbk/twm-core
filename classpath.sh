#!/usr/bin/env bash
setenv CLASSPATH lib/libstemmer.jar
setenv CLASSPATH ${CLASSPATH}:dist/jcore-lib.jar
setenv CLASSPATH ${CLASSPATH}:dist/jcore-alone.jar
