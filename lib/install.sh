#!/bin/bash

echo Intalling deps ...
echo

mvn install:install-file -Dfile=libstemmer.jar -DgroupId=org.tartarus.snowball -DartifactId=snowball -Dversion=1.0 -Dpackaging=jar || { echo 'FAILURE'; exit 1; }
echo
echo
echo SUCCESS